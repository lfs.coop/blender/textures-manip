import bpy

class TexturesManipPreferences(bpy.types.AddonPreferences):
    
    bl_idname = __package__

    whoami: bpy.props.StringProperty(
        name="Libreflow Username",
    )
    libreflow_root: bpy.props.StringProperty(
        name="Libreflow Root",
        subtype='DIR_PATH'
    )

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="The path must be written with '/' and must finish with a '/'")

        row = layout.row()
        row.prop(self, "whoami")

        row = layout.row()
        row.prop(self, "libreflow_root")


def who_am_i():
    return bpy.context.preferences.addons[__package__].preferences.whoami


def libreflow_root():
    return bpy.context.preferences.addons[__package__].preferences.libreflow_root

