bl_info = {
    "name": "Textures Manipulation",
    "description": "Create textures proxies and remap to low version",
    "author": "Les Fées Spéciales (LFS)",
    "version": (1, 0),
    "blender": (2, 91),
    "location": "Object > Properties > Textures Manipulation",
    "category": "Shading",
}


import bpy
import sys
from .textures_manip import invalid_img, unpack_textures, update_textures
from .textures_manip import fails, exporting_path
from .remap_to_low import exceptions, low_made, swap_textures
from .utils import get_paths_infos
from .preferences import TexturesManipPreferences, who_am_i, libreflow_root
from bpy.app.handlers import persistent


class UnpackTextures(bpy.types.Operator):

    """ Unpack textures in a working copy """

    bl_idname = "object.unpack_textures"
    bl_label = "Unpack Textures"

    @classmethod
    def poll(cls, context):
        for img in bpy.data.images:
            if invalid_img(img) is not None:
                return False
        infos = get_paths_infos()
        return infos['valid'] and (infos['working_copy_textures'] is not None)

    def execute(self, context):
        unpack_textures()
        return {'FINISHED'}


class UpdateTextures(bpy.types.Operator):
    
    """ Update Textures """
    
    bl_idname = "object.update_textures"
    bl_label = "Update Textures"

    @classmethod
    def poll(cls, context):
        return len(libreflow_root()) > 0

    def execute(self, context):
        update_textures()
        return {'FINISHED'}


class ReplaceInImageName(bpy.types.Operator):
    
    """ Replace In Image Name """
    
    bl_idname = "object.replace_in_image_name"
    bl_label = "Replace In Image Name"

    def execute(self, context):
        for img in bpy.data.images:
            img.name = img.name.replace(bpy.context.scene.lfs_src_img_name, bpy.context.scene.lfs_dst_img_name)
        return {'FINISHED'}



class RemapToLow(bpy.types.Operator):
    
    """ Remap To Low Textures """
    
    bl_idname = "object.remap_to_low"
    bl_label = "Remap To Low"


    @classmethod
    def poll(cls, context):
        for img in bpy.data.images:
            if (img.packed_file) or (invalid_img(img) is not None):
                return False
        infos = get_paths_infos()
        return infos['valid'] and (infos['working_copy_proxy'] is not None)


    def execute(self, context):
        exceptions.clear()
        swap_textures()
        for e in exceptions:
            print(e)
        return {'FINISHED'}


class RemoveTexture(bpy.types.Operator):
    
    """ Remove Texture """
    
    bl_idname = "object.remove_texture"
    bl_label = "Remove Texture"

    tex_name: bpy.props.StringProperty()

    def execute(self, context):
        img = bpy.data.images.get(self.tex_name)
        if img:
            bpy.data.images.remove(img)
            return {'FINISHED'}
        else:
            return {'CANCELLED'}


class TexturesManipulation(bpy.types.Panel):

    """ Textures Manipulation Panel """
    
    bl_label = "Textures Manipulation"
    bl_idname = "SCENE_PT_textures_manip"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    @classmethod
    def poll(cls, context):
        path = bpy.data.filepath.replace('\\', '/')
        return path.startswith(libreflow_root()) and ('/lib/' in path)

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.prop(bpy.context.scene, 'lfs_src_img_name', text="")
        row.prop(bpy.context.scene, 'lfs_dst_img_name', text="")

        row = layout.row()
        row.operator("object.replace_in_image_name", text="Replace")

        layout.separator()

        for img in bpy.data.images:
            split = layout.split(factor=0.1, align=True)
            split.operator("object.remove_texture", icon='TRASH', text='').tex_name = img.name
            split = split.split(align=True)
            if invalid_img(img) is not None:
                split.alert = True
            split.prop(img, 'name', text="")

        row = layout.row()
        row.operator("object.unpack_textures", icon='IMAGE')

        if len(textures_manip.exporting_path) > 0:
            row = layout.row()
            row.label(text=textures_manip.exporting_path)

        for f in fails:
            row = layout.row()
            row.label(text=f, icon='ERROR')

        row = layout.row()
        row.operator("object.remap_to_low", icon='NODE_TEXTURE')

        for e in exceptions:
            row = layout.row()
            row.label(text=e)

        row = layout.row()
        row.operator("object.update_textures", icon='SHADERFX')


@persistent
def reset_exporting_path(dumy):
    textures_manip.exporting_path = ""


def register():
    bpy.types.Scene.lfs_src_img_name = bpy.props.StringProperty(default="Source")
    bpy.types.Scene.lfs_dst_img_name = bpy.props.StringProperty(default="Destination")
    bpy.utils.register_class(RemapToLow)
    bpy.utils.register_class(UpdateTextures)
    bpy.utils.register_class(ReplaceInImageName)
    bpy.utils.register_class(RemoveTexture)
    bpy.utils.register_class(UnpackTextures)
    bpy.utils.register_class(TexturesManipulation)
    bpy.utils.register_class(TexturesManipPreferences)
    bpy.app.handlers.load_post.append(reset_exporting_path)


def unregister():
    bpy.app.handlers.load_post.remove(reset_exporting_path)
    bpy.utils.unregister_class(TexturesManipPreferences)
    bpy.utils.unregister_class(TexturesManipulation)
    bpy.utils.unregister_class(UnpackTextures)
    bpy.utils.unregister_class(ReplaceInImageName)
    bpy.utils.unregister_class(RemoveTexture)
    bpy.utils.unregister_class(UpdateTextures)
    bpy.utils.unregister_class(RemapToLow)
    del bpy.types.Scene.lfs_src_img_name
    del bpy.types.Scene.lfs_dst_img_name


if __name__ == "__main__":
    register()
