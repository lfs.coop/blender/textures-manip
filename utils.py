import bpy
import os
import re
from .preferences import who_am_i, libreflow_root

version_regex = "(v|V)[0-9]{3}"


# Only works on assets respecting file conventions
def asset_name():
    return bpy.context.scene.collection.children[0].name


# Returns the last published version contained in a directory
def last_version(path):
    if not os.path.isdir(path):
        print(f"{path} doesn't exist")
        return None
    else:
        content = [item for item in os.listdir(path) if re.search(version_regex, item)]
        content.sort()
        
        if len(content) == 0:
            return None
        else:
            return content[-1]


# Returns informations about the textures directory and the textures proxy directory
def get_paths_infos():
    path   = bpy.data.filepath.replace('\\', '/')
    lbf    = libreflow_root().replace('\\', '/')
    whoami = who_am_i()

    if (len(lbf) == 0) or (len(whoami) == 0):
        print("IDs in addon preferences are not filled")
        return {'valid': False}

    if not path.startswith(lbf):
        print(f'Path to Libreflow not found in {path}')
        return {'valid': False}
    else:
        path = path[len(lbf):] # Removing path to libreflow
        path = path.split('/')
        asset_path = []

        for item in path:
            asset_path.append(item)
            if item == asset_name():
                break

    dir_proxy    = f"shading/{'_'.join(asset_path) + '_sha_proxy'}"
    dir_textures = f"shading/{'_'.join(asset_path) + '_sha_textures'}"

    full_asset_path = f"{lbf}{'/'.join(asset_path)}"
    path_infos = {
        'general_path': full_asset_path, 
        'proxy': "/".join([full_asset_path, dir_proxy]),
        'textures': "/".join([full_asset_path, dir_textures])
    }

    if os.path.isdir(path_infos['proxy']):
        version_proxy = last_version(path_infos['proxy'])
        if version_proxy:
            path_infos['proxy_version'] = "/".join([path_infos['proxy'], version_proxy])
        else:
            path_infos['proxy_version'] = None
    else:
        path_infos['proxy_version'] = None
        path_infos['proxy'] = None


    if os.path.isdir(path_infos['textures']):
        version_textures = last_version(path_infos['textures'])
        if version_textures:
            path_infos['textures_version'] = "/".join([path_infos['textures'], version_textures])
        else:
            path_infos['textures_version'] = None

    else:
        path_infos['textures_version'] = None
        path_infos['textures'] = None


    if path_infos['proxy']:
        working_copy_proxy = "/".join([path_infos['proxy'], whoami])
        if os.path.isdir(working_copy_proxy):
            path_infos['working_copy_proxy'] = working_copy_proxy
        else:
            path_infos['working_copy_proxy'] = None
    else:
        path_infos['working_copy_proxy'] = None


    if path_infos['textures']:
        working_copy_textures = "/".join([path_infos['textures'], whoami])
        if os.path.isdir(working_copy_textures):
            path_infos['working_copy_textures'] = working_copy_textures
        else:
            path_infos['working_copy_textures'] = None
    else:
        path_infos['working_copy_textures'] = None

    path_infos['valid'] = True
    
    return path_infos