import bpy
import re
import json
import os
import sys
import shutil
from .utils import asset_name, get_paths_infos, last_version


exporting_path = ""
fails          = set()


# Returns None if the image's name is valid, the image otherwise
def invalid_img(img):

    # Expressions that textures' name must respect
    regex_neck = f"shadow_neck(_{asset_name()})?(_low)?"
    regex_jaw  = f"shadow_jaw(_{asset_name()})?(_low)?"
    regex_img  = f"siren_ch_{asset_name()}_[a-z]+(_low)?"
    regex_pikr = f"{asset_name()}_picker_bg(_low)?"
    i = img.name

    if re.fullmatch(regex_neck, i):
        return None

    elif re.fullmatch(regex_jaw, i):
        return None

    elif re.fullmatch(regex_img, i):
        return None

    elif re.fullmatch(regex_pikr, i):
        return None

    else:
        return img


def update_textures():
    for img in bpy.data.images:
        if len(img.filepath) > 0:
            path     = bpy.path.abspath(img.filepath)
            s_path   = path.split('/')
            img_name = s_path.pop()
            version  = s_path.pop()

            if not os.path.isdir("/".join(s_path)):
                print('Directory does not exist')
                continue

            s_path.append(last_version("/".join(s_path)))
            s_path.append(img_name)

            if os.path.isfile("/".join(s_path)):    
                img.filepath = bpy.path.relpath("/".join(s_path))
            else:
                continue


def unpack_textures():
    global exporting_path, fails
    fails.clear()

    dir_infos = get_paths_infos()
    if dir_infos['working_copy_textures'] is None:
        return 'CANCELLED'

    if exporting_path != dir_infos['working_copy_textures']:
        exporting_path = dir_infos['working_copy_textures']
        return 'WAITING'

    for img in bpy.data.images:
        if not '_low' in img.name:
            new_path = "/".join([dir_infos['working_copy_textures'], f'{img.name}.png'])

            if img.packed_file:
                print(f'Unpacking {img.name} to {new_path}')
                img.filepath = bpy.path.relpath(new_path)
                img.save()
                img.unpack(method='REMOVE') # Maybe to change or remove in case the path actually points on something
            else:
                try:
                    imgpath = bpy.path.abspath(img.filepath)
                except:
                    fails.add(f'Failed to unpack {img.name} (not packed and invalid path)')

                if os.path.isfile(imgpath):
                    print(f'Copying {img.name} to {new_path}')
                    shutil.copy(imgpath, new_path)
                    img.filepath = bpy.path.relpath(new_path)
                else:
                    fails.add(f'Failed to unpack {img.name} (not packed and its path doesn\'t exist)')
    return 'FINISHED'
