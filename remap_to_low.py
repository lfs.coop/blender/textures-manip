import bpy
import os
from PIL import Image
from .utils import get_paths_infos


exceptions = []
low_made   = set()


# For a material, gives all the textures nodes contained in its nodes tree
def locate_textures(material):
    if (material is not None) and (material.use_nodes) and (material.node_tree is not None):
        return [node for node in material.node_tree.nodes if node.type == 'TEX_IMAGE']
    else:
        return None


# Tries to find the corresponding proxy texture for each original texture
def path_to_proxy(node):
    path_infos = get_paths_infos()
    img = node.image

    if img is None:
        return False

    if img.name.endswith('_low'):
        return True
    
    if (path_infos is None) or (not path_infos['valid']):
        print(f'Failed to remap for node {node.name}')
        return False

    if len(img.filepath) == 0: # Trying to intercept "Render Result", ...
        return False

    path_original = bpy.path.abspath(img.filepath)
    if not os.path.isfile(path_original):
        print("File not found on file system")
        return False

    low_name = img.name + "_low"
    low_path = f"{path_infos['working_copy_proxy']}/{low_name}.png"

    if low_name in bpy.data.images:
        node.image = bpy.data.images[low_name]
        return True

    src_img = Image.open(path_original)
    width, height = src_img.size
    if (width/10 > 100) and (height/10 > 100):
        dst_img = src_img.resize((int(width/10), int(height/10)))
    else:
        dst_img = src_img

    dst_img.save(low_path)
    
    img_low = bpy.data.images.load(filepath=low_path)
    img_low.filepath = bpy.path.relpath(low_path)
    img_low.name = low_name
    node.image = img_low

    return True

# For each mesh of the _low collection, duplicates the held material and gives it a low res texture
def swap_textures():
    low_collection = None
    mapped = {}
    candidates = [c for c in bpy.data.collections if c.name.endswith('_low')]

    if len(candidates) == 0:
        return
    else:
        low_collection = candidates[0]
        
    for obj in low_collection.all_objects:
        if obj.name.endswith('_low'):
            if obj.material_slots is not None:
                for slot in obj.material_slots:
                    if (slot is not None) and (slot.material is not None):
                        if slot.material.name.endswith('_low'):
                            # If the material is already a "_low res" material, we don't have anything to do
                            pass
                        elif slot.material in mapped:
                            # If it's a high res material already met, we assign it to the already created low version
                            slot.material = mapped[slot.material]
                        else:
                            old_m = slot.material
                            new_m = old_m.copy()
                            new_m.name = old_m.name + "_low"

                            tex_nodes = locate_textures(new_m)

                            if tex_nodes is None:
                                print('No texture found for {new_m.name}')
                                continue

                            for tex_node in tex_nodes:
                                if tex_node.image is None:
                                    print('Invalid image found')
                                    continue
                                else:
                                    if not path_to_proxy(tex_node):
                                        exceptions.append(tex_node.image.name)
                                    else:
                                        slot.material = new_m
                                        mapped[old_m] = new_m